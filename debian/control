Source: python-jieba
Section: python
Priority: optional
Maintainer: Debian Chinese Team <chinese-developers@lists.alioth.debian.org>
Uploaders:
 Yangfl <mmyangfl@gmail.com>,
 Boyuan Yang <byang@debian.org>,
Build-Depends:
 debhelper-compat (= 12),
 dh-python,
 python3,
 python3-setuptools,
Rules-Requires-Root: no
Standards-Version: 4.5.0
Homepage: https://github.com/fxsjy/jieba
Vcs-Git: https://salsa.debian.org/chinese-team/python-jieba.git
Vcs-Browser: https://salsa.debian.org/chinese-team/python-jieba

Package: python-jieba-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
Multi-Arch: foreign
Description: Jieba Chinese text segmenter (common documentation)
 "Jieba" (Chinese for "to stutter")is a high-accuracy Chinese text segmenteran
 based on HMM-model and Viterbi algorithm. It uses dynamic programming to find
 the most probable combination based on the word frequency.
 .
 This is the common documentation package.

Package: python3-jieba
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-jieba-doc,
Description: Jieba Chinese text segmenter (Python 3)
 "Jieba" (Chinese for "to stutter")is a high-accuracy Chinese text segmenteran
 based on HMM-model and Viterbi algorithm. It uses dynamic programming to find
 the most probable combination based on the word frequency.
 .
 It supports three types of segmentation mode:
  * Accurate Mode attempts to cut the sentence into the most accurate
    segmentations, which is suitable for text analysis.
  * Full Mode gets all the possible words from the sentence. Fast but not
    accurate.
  * Search Engine Mode, based on the Accurate Mode, attempts to cut long words
    into several short words, which can raise the recall rate. Suitable for
    search engines.
 Traditional Chinese and customized dictionaries are also supported.
 .
 This package installs the library for Python 3.
